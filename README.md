# MiniCommerce

Mini commerce project using springboot

Deskripsi Project: Mini Commerce ini merupakan aplikasi sederhana yang dirancang untuk mendemonstrasikan fungsionalitas dasar dari platform e-commerce. Proyek ini mencakup fitur-fitur dasar seperti penambahan user, penambahan product, penambahan order, dan penambahan kategori produk.

Fitur-Fitur: Dalam project ini, total terdapat 16 fitur yang diterapkan ke dalam 16 endpoints yang dapat digunakan, diantaranya:

- Get Products (/products) GET
- Get Category (/products/category) GET
- Get Users (/users) GET
- Get Orders (/orders) GET
- Get Users Order (/orders/user/{user_id}) GET
- Get Products Below Price (/products/belowPrice/{price}) GET
- Get Products Above Price (/products/abovePrice/{price}) GET
- Get Products Name Ascending (/products/sortNameAscending) GET
- Get Products Name Descending (/products/sortNameDescending) GET
- Get Products Price Ascending (/products/sortPriceAscending) GET
- Get Products Price Descending (/products/sortPriceDescending) GET
- Get Products By Category (/products/category/{category_id}) GET
- Add Products (/products) POST
- Add Categories (/products/category) POST
- Add Users (/users) POST
- Add Orders (/orders) POST


File untuk database dapat diakses di link berikut: https://drive.google.com/file/d/1fMFSq9uH8J-zXdVgOkpm0bDQePkk27rS/view?usp=drive_link
