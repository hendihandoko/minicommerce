package com.ecommerce.service;

import com.ecommerce.model.Category;
import com.ecommerce.model.Product;
import com.ecommerce.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<Product> getAllProducts() {
        return productRepository.getAllProducts();
    }

    public List<Category> getAllProductsCategory() {
        return productRepository.getAllCategory();
    }

    public List<Product> addProduct(List<Product> product) {
        return productRepository.addProducts(product);
    }

    public List<Category> addCategory(List<Category> category) {
        return productRepository.addCategories(category);
    }


    public List<Product> getProductsByCategory(Long categoryId) {
        return productRepository.findProductsByCategoryId(categoryId);
    }

}
