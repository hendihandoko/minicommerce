package com.ecommerce.repository;

import com.ecommerce.model.Product;
import com.ecommerce.model.User;

import java.util.List;

public interface UserRepository {
    List<User> getAllUsers();
    List<User> addUsers(List<User> user);
}
