package com.ecommerce.repository;

import com.ecommerce.model.Order;
import com.ecommerce.model.User;

import java.util.List;
import java.util.Map;

public interface OrderRepository {
    List<Order> getAllOrders();
    List<Order> addOrders(List<Map<String, Long>> orders);
    List<Order> findByUserId(Long userId);
}
