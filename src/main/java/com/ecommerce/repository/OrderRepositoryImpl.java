package com.ecommerce.repository;

import com.ecommerce.model.Order;
import com.ecommerce.model.Product;
import com.ecommerce.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class OrderRepositoryImpl implements OrderRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Order> getAllOrders() {
        String sql = "SELECT o.id as order_id, u.username, u.id, u.email, p.id as product_id, p.name, p.price, p.category_id " +
                "FROM orders o " +
                "JOIN users u ON o.user_id = u.id " +
                "JOIN products p ON o.product_id = p.id";

        return jdbcTemplate.query(sql, (resultSet, rowNum) -> {
            User user = new User();
            user.setId(resultSet.getLong("id"));
            user.setUsername(resultSet.getString("username"));
            user.setEmail(resultSet.getString("email"));

            Product product = new Product();
            product.setId(resultSet.getLong("product_id"));
            product.setName(resultSet.getString("name"));
            product.setPrice(resultSet.getDouble("price"));
            product.setCategoryId(resultSet.getLong("category_id"));

            return new Order(resultSet.getLong("order_id"), user, product);
        });
    }

    @Override
    public List<Order> addOrders(List<Map<String, Long>> orders) {
        String sql = "INSERT INTO orders (user_id, product_id) VALUES (?, ?)";

        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Map<String, Long> order = orders.get(i);
                ps.setLong(1, order.get("userId"));
                ps.setLong(2, order.get("productId"));
            }

            @Override
            public int getBatchSize() {
                return orders.size();
            }
        });

        // Ambil pesanan yang baru ditambahkan dari database dan kembalikan
        String selectSql = "SELECT o.id as order_id, u.id as user_id, u.username, p.id as product_id, p.name, p.price, p.category_id FROM orders o " +
                "JOIN users u ON o.user_id = u.id " +
                "JOIN products p ON o.product_id = p.id " +
                "WHERE o.user_id = ? AND o.product_id = ?";
        return orders.stream()
                .map(order -> {
                    // Ambil data yang baru saja dimasukkan dari database
                    return jdbcTemplate.query(
                            selectSql,
                            new Object[]{order.get("userId"), order.get("productId")},
                            (resultSet, rowNum) -> {
                                // Membuat objek Order dari hasil query
                                Order newOrder = new Order();
                                newOrder.setId(resultSet.getLong("order_id"));

                                // Membuat objek User dari hasil query
                                User user = new User();
                                user.setId(resultSet.getLong("user_id"));
                                user.setUsername(resultSet.getString("username"));
                                newOrder.setUser(user);

                                // Membuat objek Product dari hasil query
                                Product product = new Product();
                                product.setId(resultSet.getLong("product_id"));
                                product.setName(resultSet.getString("name"));
                                product.setPrice(resultSet.getDouble("price"));
                                product.setCategoryId(resultSet.getLong("category_id"));
                                newOrder.setProduct(product);

                                return newOrder;
                            }
                    );
                })
                .flatMap(List::stream)
                .collect(Collectors.toList());
    }

    @Override
    public List<Order> findByUserId(Long userId) {
        String sql = "SELECT o.id as order_id, p.id as product_id, p.name, p.price, p.category_id " +
                "FROM orders o " +
                "JOIN products p ON o.product_id = p.id " +
                "WHERE o.user_id = ?";

        return jdbcTemplate.query(sql, new Object[]{userId}, (resultSet, rowNum) -> {
            Order order = new Order();
            order.setId(resultSet.getLong("order_id"));

            Product product = new Product();
            product.setId(resultSet.getLong("product_id"));
            product.setName(resultSet.getString("name"));
            product.setPrice(resultSet.getDouble("price"));
            product.setCategoryId(resultSet.getLong("category_id"));

            order.setProduct(product);
            return order;
        });
    }
}
