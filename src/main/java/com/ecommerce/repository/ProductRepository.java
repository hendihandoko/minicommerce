package com.ecommerce.repository;

import com.ecommerce.model.Category;
import com.ecommerce.model.Product;

import java.util.List;

public interface ProductRepository {
    List<Product> getAllProducts();
    List<Category> getAllCategory();
    List<Product> findProductsByCategoryId(Long categoryId);
    List<Product> addProducts(List<Product> product);
    List<Category> addCategories(List<Category> category);
}
