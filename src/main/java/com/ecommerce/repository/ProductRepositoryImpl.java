package com.ecommerce.repository;

import com.ecommerce.model.Category;
import com.ecommerce.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Product> getAllProducts() {
        String sql = "SELECT * FROM products";
        return jdbcTemplate.query(sql, (resultSet, rowNum) -> {
                    Product product = new Product();
                    product.setId(resultSet.getLong("id"));
                    product.setName(resultSet.getString("name"));
                    product.setPrice(resultSet.getDouble("price"));
                    product.setCategoryId(resultSet.getLong("category_id"));
                    return product;
                }
        );
    }

    @Override
    public List<Category> getAllCategory() {
        String sql = "SELECT * FROM category";
        return jdbcTemplate.query(sql, (resultSet, rowNum) -> {
                    Category category = new Category();
                    category.setId(resultSet.getLong("id"));
                    category.setCategory_name(resultSet.getString("category_name"));
                    return category;
                }
        );
    }
    public List<Product> findProductsByCategoryId(Long categoryId) {
        String sql = "SELECT * FROM products WHERE category_id = ?";
        return jdbcTemplate.query(sql, new Object[]{categoryId}, (resultSet, rowNum) -> {
            Product product = new Product();
            product.setId(resultSet.getLong("id"));
            product.setName(resultSet.getString("name"));
            product.setPrice(resultSet.getDouble("price"));
            product.setCategoryId(resultSet.getLong("category_id"));
            return product;
        });
    }

    public List<Product> addProducts(List<Product> products) {
        String sql = "INSERT INTO products (name, price, category_id) VALUES (?, ?, ?)";

        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Product product = products.get(i);
                ps.setString(1, product.getName());
                ps.setDouble(2, product.getPrice());
                if (product.getCategoryId() != null) {
                    ps.setLong(3, product.getCategoryId());
                } else {
                    ps.setNull(3, java.sql.Types.BIGINT);
                }
            }

            @Override
            public int getBatchSize() {
                return products.size();
            }
        });
        return products;
    }

    public List<Category> addCategories(List<Category> categories) {
        String sql = "INSERT INTO category (category_name) VALUES (?)";

        jdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i) throws SQLException {
                Category category = categories.get(i);
                ps.setString(1, category.getCategory_name());
            }
            @Override
            public int getBatchSize() {
                return categories.size();
            }
        });
        return categories;
    }
}
