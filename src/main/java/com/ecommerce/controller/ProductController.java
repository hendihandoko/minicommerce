package com.ecommerce.controller;

import com.ecommerce.model.Category;
import com.ecommerce.model.Product;
import com.ecommerce.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public List<Product> getAllProducts() {
        return productService.getAllProducts();
    }

    @GetMapping("/category")
    public List<Category> getAllCategory() {
        return productService.getAllProductsCategory();
    }

    @PostMapping("/category")
    public List<Category> addCategory(@RequestBody List<Category> category) {
        return productService.addCategory(category);
    }

    @GetMapping("/belowPrice/{price}")
    public List<Product> getProductsBelowPrice(@PathVariable Double price) {
        List<Product> allProducts = productService.getAllProducts();
        return allProducts.stream()
                .filter(product -> product.getPrice() < price)
                .collect(Collectors.toList());
    }

    @GetMapping("/abovePrice/{price}")
    public List<Product> getProductsAbovePrice(@PathVariable Double price) {
        List<Product> allProducts = productService.getAllProducts();
        return allProducts.stream()
                .filter(product -> product.getPrice() > price)
                .collect(Collectors.toList());
    }

    @GetMapping("/sortNameAscending")
    public List<Product> getProductsNameAscending() {
        List<Product> allProducts = productService.getAllProducts();
        return allProducts.stream()
                .sorted(Comparator.comparing(Product::getName))
                .collect(Collectors.toList());
    }

    @GetMapping("/sortNameDescending")
    public List<Product> getProductsNameDescending() {
        List<Product> allProducts = productService.getAllProducts();
        return allProducts.stream()
                .sorted(Comparator.comparing(Product::getName).reversed())
                .collect(Collectors.toList());
    }

    @GetMapping("/sortPriceAscending")
    public List<Product> getPriceAscending() {
        List<Product> allProducts = productService.getAllProducts();
        return allProducts.stream()
                .sorted(Comparator.comparingDouble(Product::getPrice))
                .collect(Collectors.toList());
    }

    @GetMapping("/sortPriceDescending")
    public List<Product> getPriceDescending() {
        List<Product> allProducts = productService.getAllProducts();
        return allProducts.stream()
                .sorted(Comparator.comparingDouble(Product::getPrice).reversed())
                .collect(Collectors.toList());
    }


    @PostMapping
    public List<Product> addProduct(@RequestBody List<Product> product) {
        return productService.addProduct(product);
    }

    @GetMapping("/category/{categoryId}")
    public List<Product> getProductsByCategory(@PathVariable Long categoryId) {
        return productService.getProductsByCategory(categoryId);
    }
}
